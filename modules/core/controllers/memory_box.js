'use strict';

/**
 * @ngdoc object
 * @name core.Controllers.HomeController
 * @description Home controller
 * @requires ng.$scope
 */
angular
    .module('core')
    .controller('MemoryBoxController', ['$scope','$rootScope',
        function($scope,$rootScope) {
            
           $rootScope.page_heading = "Memory Box"; 
        }
    ]);

