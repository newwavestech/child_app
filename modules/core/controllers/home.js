/**
 * @ngdoc object
 * @name core.Controllers.HomeController
 * @description Home controller
 * @requires '$scope','$rootScope','$state','restAPIService'
 */
angular.module('core').controller('HomeController', ['$scope','$cookies','$rootScope','$state','$window','Constant','$location','restAPICore',
    function($scope,$cookies,$rootScope,$state,$window,Constant,$location,restAPICore) {
		var user= $cookies.get('user');
                $rootScope.menuShow =false; 
                $rootScope.addToCartShow =false; 
                
                $rootScope.page_heading ="Child App";
                
                var session = angular.fromJson($window.localStorage.getItem("user_session"));
                
                if($scope.is_session ==true) 
                {
                    $scope.customer_id = session.customer_id;
                    $scope.token = session.token;           


                    $scope.cust_info = {"customer_id":$scope.customer_id,"token":$scope.token};
                
                    restAPICore.getDashboardCount($scope.cust_info).success(function(response)
                    {

                        if (response["status"] == "success")
                        {
                            $scope.OutStandingAmount = response["result"]["OutStandingAmount"];
                            $scope.payment_val = response["result"]["payments"];
                            
                            if($scope.payment_val!="")
                            {
                                // LATEST PAYMENT
                                $scope.latest_payment = $scope.payment_val[0]['payment'];
                                $scope.latest_payment_date = $scope.payment_val[0]['date'];

                                // LAST PAYMENT
                                $scope.last_payment = $scope.payment_val[1]['payment'];
                                $scope.last_payment_date = $scope.payment_val[1]['date'];
                            }
                            else
                            {
                                $scope.latest_payment = "--";
                                $scope.last_payment = "--";
                            }    
                            
                            
                            $scope.showLoader(true);
                        }
                        else
                        {
                            $scope.showLoader(true);
                        }

                    }).error(function(response) {                
                        $scope.showLoader(true);
                    });
                }    
                
    }
])

angular.module('core').controller('ForgotPasswordController', ['$scope','$cookies','$rootScope','$state','$window','Constant',
    function($scope,$cookies,$rootScope,$state,$window,Constant) {
		
		var user= $cookies.get('user');
		$scope.userObj ={};
		
		if (user == "" || user == undefined) {
			$state.go('home');
		}else{
			$rootScope.user = JSON.parse(user);
			$window.sessionStorage.token = $rootScope.user.result.token;
		}
		
		if($state.current.name === 'logout'){
			logout();
		}
		
		
            
         /* 
         * 
         * @returns {undefined}
         */
            
          
    }
]);
