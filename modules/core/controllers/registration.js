'use strict';

/**
 * @ngdoc object
 * @name core.Controllers.HomeController
 * @description Home controller
 * @requires ng.$scope
 */
angular
        .module('core')
        .controller('RegistrationController', ['$scope', 'restAPICore', '$state','$rootScope','$window','Constant',
            function($scope, restAPICore, $state,$rootScope,$window,Constant) {

                $rootScope.page_heading = "Parent Details";
                
            }
        ]);
