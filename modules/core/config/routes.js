'use strict';

/**
 * @ngdoc object
 * @name core.config
 * @requires ng.$stateProvider
 * @requires ng.$urlRouterProvider
 * @description Defines the routes and other config within the core module
 */
angular
        .module('core')
        .config(['$stateProvider',
            '$urlRouterProvider',
            function($stateProvider, $urlRouterProvider) {

                $urlRouterProvider.otherwise('/');

                /**
                 * @ngdoc event
                 * @name core.config.route
                 * @eventOf core.config
                 * @description
                 *
                 * Define routes and the associated paths
                 *
                 * - When the path is `'/'`, route to home
                 * */
                $stateProvider

                        .state('main_dashboard', {
                            url: '/main_dashboard',
                            templateUrl: 'modules/core/views/main_dashboard.html',
                            controller: 'MainDashboardController'
                        })

                        .state('home', {
                            url: '/',
                            templateUrl: 'modules/core/views/home.html',
                            controller: 'HomeController'
                        })
                        .state('login', {
                            url: '/login',
                            templateUrl: 'modules/core/views/login.html',
                            controller: 'LoginController'
                        })
                        .state('logout', {
                            url: '/logout',
                            templateUrl: 'modules/core/views/login.html',
                            controller: 'LoginController'
                        })
                        .state('otp', {
                            url: '/otp',
                            templateUrl: 'modules/core/views/otp.html',
                            controller: 'OtpController'
                        })
                        .state('enterOtp', {
                            url: '/enterOtp/:otp_for?',
                            templateUrl: 'modules/core/views/enterOtp.html',
                            controller: 'EnterotpController'
                        })
                        .state('referralCode', {
                            url: '/referralCode',
                            templateUrl: 'modules/core/views/referralCode.html',
                            controller: 'ReferralcodeController'
                        })
                        .state('userinfo', {
                            url: '/userinfo',
                            templateUrl: 'modules/core/views/userinfo.html',
                            controller: 'UserinfoController'
                        })
                        .state('about_us', {
                            url: '/about_us',
                            templateUrl: 'modules/core/views/about_us.html',
                            controller: 'AboutUsController'
                        })
                         .state('addcart', {
                            url: '/addcart',
                            templateUrl: 'modules/core/views/addcart.html',
                            controller: 'AddCartController'
                        })
                        .state('howitworks', {
                            url: '/howitworks',
                            templateUrl: 'modules/core/views/howitworks.html',
                            controller: 'HowitworksController'
                        })
                        .state('customercare', {
                            url: '/customercare',
                            templateUrl: 'modules/core/views/customercare.html',
                            controller: 'CustomercareController'
                        })
                        .state('registration', {
                            url: '/registration',
                            templateUrl: 'modules/core/views/registration.html',
                            controller: 'RegistrationController'
                        })
                        .state('update_profile', {
                            url: '/update_profile',
                            templateUrl: 'modules/core/views/update_profile.html',
                            controller: 'UpdateProfileController'
                        })
                         .state('articles_blogs', {
                            url: '/articles_blogs',
                            templateUrl: 'modules/core/views/articles_blogs.html',
                            controller: 'ArticlesBlogsController'
                        })
                          .state('domain', {
                            url: '/domain',
                            templateUrl: 'modules/core/views/domain.html',
                            controller: 'DomainController'
                        })                       
                       .state('child_acitivity_progress', {
                            url: '/child_acitivity_progress',
                            templateUrl: 'modules/core/views/child_acitivity_progress.html',
                            controller: 'ChildAcitivityProgressController'
                        })  
                            .state('events', {
                            url: '/events',
                            templateUrl: 'modules/core/views/events.html',
                            controller: 'EventsController'
                        })
                             .state('grow_chart', {
                            url: '/grow_chart',
                            templateUrl: 'modules/core/views/grow_chart.html',
                            controller: 'GrowChartController'
                        })
                              .state('games', {
                            url: '/games',
                            templateUrl: 'modules/core/views/games.html',
                            controller: 'GamesController'
                        })
                        .state('leaderboard', {
                            url: '/leaderboard',
                            templateUrl: 'modules/core/views/leaderboard.html',
                            controller: 'LeaderboardController'
                        })
                        .state('memory_box', {
                            url: '/memory_box',
                            templateUrl: 'modules/core/views/memory_box.html',
                            controller: 'MemoryBoxController'
                        })
                        .state('subscription', {
                            url: '/subscription',
                            templateUrl: 'modules/core/views/subscription.html',
                            controller: 'SubscriptionController'
                        })
                        .state('profile_edit', {
                            url: '/profile_edit',
                            templateUrl: 'modules/core/views/profile_edit.html',
                            controller: 'ProfileEditController'
                        })
                        .state('counselling', {
                            url: '/counselling',
                            templateUrl: 'modules/core/views/counselling.html',
                            controller: 'CounsellingController'
                        })
                        .state('welcome', {
                            url: '/welcome',
                            templateUrl: 'modules/core/views/welcome.html',
                            controller: 'WelcomeController'
                        })
                        .state('vaccine_chart', {
                            url: '/vaccine_chart',
                            templateUrl: 'modules/core/views/vaccine_chart.html',
                            controller: 'VaccineChartController'
                        })
                         .state('child_acitivity_progress_chart', {
                            url: '/child_acitivity_progress_chart',
                            templateUrl: 'modules/core/views/child_acitivity_progress_chart.html',
                            controller: 'ChildAcitivityProgressChartController'
                        })
                         .state('congratulation', {
                            url: '/congratulation',
                            templateUrl: 'modules/core/views/congratulation.html',
                            controller: 'CongratulationController'
                        })
                         .state('child_domain_plain', {
                            url: '/child_domain_plain',
                            templateUrl: 'modules/core/views/child_domain_plain.html',
                            controller: 'ChilddomainPlainController'
                        })
                        .state('child_domain', {
                            url: '/child_domain',
                            templateUrl: 'modules/core/views/child_domain.html',
                            controller: 'ChilddomainController'
                        })
                          .state('child_subdomain', {
                            url: '/child_subdomain',
                            templateUrl: 'modules/core/views/child_subdomain.html',
                            controller: 'ChildSubdomainController'
                        })
                          .state('child_activity', {
                            url: '/child_activity',
                            templateUrl: 'modules/core/views/child_activity.html',
                            controller: 'ChildActivityController'
                        })

                        .state('arrange_board', {
                            url: '/arrange_board',
                            templateUrl: 'modules/core/views/arrange_board.html',
                            controller: 'ArrangeBoardController'
                        })

                        .state('rewards', {
                            url: '/rewards',
                            templateUrl: 'modules/core/views/rewards.html',
                            controller: 'RewardsController'
                        })
                        .state('fill_child_details', {
                            url: '/fill_child_details',
                            templateUrl: 'modules/core/views/fill_child_details.html',
                            controller: 'FillChildDetailsController'
                        })
                        .state('demo_video', {
                            url: '/demo_video',
                            templateUrl: 'modules/core/views/demo_video.html',
                            controller: 'DemoVideoController'
                        })
                        .state('rhymes_stories', {
                            url: '/rhymes_stories',
                            templateUrl: 'modules/core/views/rhymes_stories.html',
                            controller: 'RhymesStoriesController'
                        })
                        .state('child_profile', {
                            url: '/child_profile',
                            templateUrl: 'modules/core/views/child_profile.html',
                            controller: 'ChildProfileController'
                        })
                         .state('child_list', {
                            url: '/child_list',
                            templateUrl: 'modules/core/views/child_list.html',
                            controller: 'ChildListController'
                        })
                         .state('child_list2', {
                            url: '/child_list2',
                            templateUrl: 'modules/core/views/child_list2.html',
                            controller: 'ChildList2Controller'
                        })

                    

            }
        ]);
